import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Control from './Control'
import registerServiceWorker from './registerServiceWorker';

//reveal
import './js/libs/reveal/css/reveal.css'
import './js/libs/reveal/css/theme/black.css'

import './js/libs/reveal/lib/js/head.min'



ReactDOM.render(<Control />, document.getElementById('control-div'));

ReactDOM.render(<App />, document.getElementById('reveal'));

registerServiceWorker();
