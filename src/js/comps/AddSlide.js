import React, {Component} from 'react';
import {Child, Parent} from './Components'

class AddSlide extends Component {

    constructor(props) {
        super(props);
        this.children = [];
        this.state = {
            numberOfSlides: 0
        }
    }

    addSlide() {
        this.setState({
            numberOfSlides: this.state.numberOfSlides++
        })
    }

    emptyChildren() {
        this.children.length = 0;
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.numberOfSlides) {
            this.emptyChildren();
            for(let i=0; i<nextProps.numberOfSlides;i++) {
                this.children.push(<Child key={i+new Date()} number={i}/>);
            }
        }
    }
    render() {
        return (
            <Parent >
                {this.children}
            </Parent>
        );
    }
}

export default AddSlide