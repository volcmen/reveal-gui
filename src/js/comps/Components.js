import React from 'react';
import emptyIMG from '../../img/empty.jpg'


export class Parent extends React.Component {
    render() {
        return (
            <div className="slides">
                <section>test</section>
                {this.props.children}
            </div>
        )
    }
}

export class Child extends React.Component {
    render() {
        return (
            <section>
                <h2>It's a child {this.props.number}</h2>
                <img src={emptyIMG} alt=""/>
            </section >)
    }
}