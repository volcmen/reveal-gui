import React, { Component } from 'react';
import Button from './js/comps/Button';
import AddSlide from "./js/comps/AddSlide";

import './css/comps/Button.css'

let clsAddSlide = new AddSlide();


class Control extends Component {


    render() {
        return (
            <div>
                <Button onClick={clsAddSlide.addSlide.bind(this)}/>
            </div>
        )
    }
}


export default Control;
